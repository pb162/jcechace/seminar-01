# Seminar 01: Introduction 
This seminar serves to setup the environment and get a basic undestanding of how java works. 

## Environment Setup

In this course you will need the following tools

- JDK 17
- Git VCS
- IntelliJ Idea IDE

Follow these steps in order to setup your computer

- [Faculty](https://pv168.pages.fi.muni.cz/guides/install-fi.html)
- [Unix/MacOS](https://pv168.pages.fi.muni.cz/guides/install-unix.html)
- [Windows](https://pv168.pages.fi.muni.cz/guides/install-windows)

### Git
You have likely encountered git already at the faculty. If not then the following resources might be helpful 

- [Nice and simple git tutorial](http://rogerdudler.github.io/git-guide/)
- [The official docs](https://www.git-scm.com/doc)


*Note: When working with Git it's recommended to have a [working SSH key](https://docs.gitlab.com/ee/user/ssh.html)*

### 1. Clone it
Clone this repository 

```bash
# With ssh
git clone git@gitlab.fi.muni.cz:pb162/jcechace/seminar-01.git

# With https
git clone https://gitlab.fi.muni.cz/pb162/jcechace/seminar-01.git
```

## Java Application

In this section we will demonstrate what it means to create a runnable Java application and what tools are needed in the process. 

### 1.  Hello World
Under `src/main/java` is a package structure of a very simple hello world program. Locate the `Main` class and finish its implementation


### 2. Build and run the App

Java compiler `javac` is used to build java sources.
```bash
# compile all *.java files under src/main 
javac -sourcepath src/main/java src/main/java/cz/muni/fi/pb162/hello/app/*.java -d target
```

*Optional: read more about compilation in this [article](https://www.baeldung.com/javac-compile-classes-directory)*

```bash
# run either of the two entry points 
java -cp target cz.muni.fi.pb162.hello.app.Main
java -cp target cz.muni.fi.pb162.hello.app.MainGreeter
```

### 3. Package and run the App
Having a bunch of `*.class` files in potentially complex package structure is not practical. To neatly tuck all classes together we can use `jar` file instead.

```bash 
# create jar file 
jar cf greeter.jar -C target .
```

Now you can try to run the `jar`.

```bash
# run as executable jar
java -jar greeter.jar 

# run by putting the jar on classpath and specifying entry point 
java -cp greeter.jar cz.muni.fi.pb162.hello.app.Main
java -cp greeter.jar cz.muni.fi.pb162.hello.app.MainGreeter
```

Let us make the `jar` executable by defining its entry point.

```bash
# create executable jar
jar cfe greeter.jar cz.muni.fi.pb162.hello.app.Main greeter.jar -C target .
```

Now you can run it as executable jar.


## Maven
Apache maven is probably the most used build tool in the world of java.  It simplifies the build process by providing a uniform build system with reasonable defaults,
The tool is highly configurable and has the ability to be extended via plugins.


In this course you will only ever be required to use maven by executing clearly specified commands. However, as with all code execution, it is a good (security) practice to understand what's going to happen. 
### Directory Layout
The following is the standard layout for Maven projects
```
project
|- src
    |- main
        |- java
        |- ressources
    |- test
        |- java
        |- ressources
|- pom.xml
```

### Project Descriptor and GAV
Maven project uses [project descriptor](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#Minimal_POM) named `pom.xml` to define the entire build process.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cz.muni.fi.pv168</groupId>
    <artifactId>greeter</artifactId>
    <version>1.0-SNAPSHOT</version>
</project>
```

### 1. Build, Package and Run the App
Simply execute 

```bash
# Clean previous build and do all steps required to create an executable jar 
mvn clean package

# Run the executable jar
java -jar target/greeter.jar 
```