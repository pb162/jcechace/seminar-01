package cz.muni.fi.pb162.hello.tools;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public final class GreeterTest {

    private Greeter greeter;

    @BeforeEach
    public void setup() {
        greeter = new Greeter();
    }

    @Test
    public void shouldHaveCorrectDefaultTemplate() {
        var message = greeter.greeting("Students");

        Assertions.assertThat(message).isEqualTo("Hello, Students!");
    }
}
