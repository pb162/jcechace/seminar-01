package cz.muni.fi.pb162.hello.app;

import cz.muni.fi.pb162.hello.tools.Greeter;


public final class MainGreeter {

    public static void main(String[] args) {
        var greeter = new Greeter();
        var message = greeter.greeting("Greeter");

        System.out.println(message);
    }
    
}
