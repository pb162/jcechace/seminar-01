package cz.muni.fi.pb162.hello.tools;


/**
 * Simple class used to greet anyone and anything
 */
public class Greeter {

    private String template;

    /**
     * Creates greeter instance with default message template 
     */
    public Greeter() {
        this("Hello, %s!");
    }

    /**
     * Creates greeter instance with custom message template ({@link String#formatted(Object...)})
     * 
     * @param template message template
     */
    public Greeter(String template) {
        this.template = template;
    }

    /**
     * Prints greeting message 
     * 
     * @param args parameters passed to templates
     */
    public String greeting(Object... args) {
        var message = template.formatted(args);
        return message;
    }
}
